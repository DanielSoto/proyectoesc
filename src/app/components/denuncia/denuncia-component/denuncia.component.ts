import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { DenunciaModalComponent } from '../denuncia-modal/denuncia-modal.component';

@Component({
  selector: 'app-denuncia',
  templateUrl: './denuncia.component.html',
  styleUrls: ['./denuncia.component.css']
})
export class DenunciaComponent implements OnInit {

  public lstWizard = [
    {
        "icono": "glyphicon glyphicon-user",
        "activo": true,
        "id": "denuncia"
    },
    {
        "icono": "glyphicon glyphicon-picture",
        "activo": false,
        "id": "evidencia"
    },
    {
        "icono": "glyphicon glyphicon-eye-open",
        "activo": false,
        "id": "preview"
    }
    ,
    {
        "icono": "glyphicon glyphicon-ok",
        "activo": false,
        "id": "CorrectoFRM"
    }
];

public page = "denuncia";
denuncia:any;  // obj con el que puedes enviar información hacia el modal;


constructor(public dialog: MatDialog) { }

ngOnInit() {
    this.denuncia ='Este modal si funciona :D '; // aqui puedes enviar lo que necesites, objetos, arreglos, texto;
}

//Abrir checklist modal
siguienteAnterio(id) {
    for (let index in this.lstWizard) {
        this.lstWizard[index].activo = false;
        if(id==this.lstWizard[index].id){
            this.lstWizard[index].activo = true;
        }
    }
    this.page=id;
}

openDialog(): void {
    const dialogRef = this.dialog.open(DenunciaModalComponent, {
      width: '1000px',
      data: this.denuncia
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('el modal fue cerrado'); // Aqui podrias recibir datos desde el modal, con el result;
    });
  }

}
