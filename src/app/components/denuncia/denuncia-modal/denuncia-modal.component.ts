import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-denuncia-modal',
  templateUrl: './denuncia-modal.component.html',
  styleUrls: ['./denuncia-modal.component.css']
})
export class DenunciaModalComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<DenunciaModalComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) { }

  ngOnInit() {
    console.log(this.data); // this.data es la info que llega al modal, que enviaste desde la función que lo invoca;
  }

  onNoClick(): void {
    this.dialogRef.close();
  }
}
