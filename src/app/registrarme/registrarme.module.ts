import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { RegistrarmeComponent } from '../registrarme/registrarme.component';
import { AppComponent } from '../app.component';

@NgModule({
  declarations: [
    AppComponent, 
    RegistrarmeComponent,
  ],
  imports: [
    CommonModule, BrowserModule, FormsModule, NgModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})

export class AppModule { }