import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegistrarmeComponent } from './registrarme.component';

describe('RegistrarmeComponent', () => {
  let component: RegistrarmeComponent;
  let fixture: ComponentFixture<RegistrarmeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegistrarmeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegistrarmeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
