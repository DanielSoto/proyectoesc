import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ModalIndexComponent } from '../modal-index/modal-index.component';
import { HttpClient } from '@angular/common/http';
import swal from 'sweetalert2';

@Component({
  selector: 'app-registrarme',
  templateUrl: './registrarme.component.html',
  styleUrls: ['./registrarme.component.css'],
  encapsulation: ViewEncapsulation.None
})

export class RegistrarmeComponent implements OnInit {

  public objRegistro = {
    "nombres": null,
    "apellidos": null,
    "correo": null,
    "sexo": null,
    "usuario": null,
    "contrasena": null
  }
  public data;
  public lstprueba = [];
  public page = "denuncia";
  public msjVisibleApellido = false;
  public msjVisibleNombre = false;
  public msjErrorCorreo = false;
  denuncia: any;
  switchGenero: any;
  confirmacion: any;

  constructor(public dialog: MatDialog, private http: HttpClient) { }

  ngOnInit() {
    this.validacionCamposVacios();
  }

  insertarUsuarios = function () {
    (!this.switchGenero) ? this.objRegistro.sexo = 'H' : this.objRegistro.sexo = 'M'
    if (this.validacionCamposVacios()) {
      this.http.post('http://localhost:8080/ccsoft/API/RegistroUsuarios/insertDatosRegistro', this.objRegistro).
        subscribe(function (response) {
          swal.fire('¡Éxito!', 'Registro de usuario exitoso.', 'success')
        },
          function (error) {
            swal.fire('¡Advertencia!', error, 'error')
          });
      this.limpiarCampos();
    } else {
      swal.fire('¡Advertencia!', 'Posibles errores: Campos vacíos, formato de nombre, apellido y/o correo incorrecto, contraseñas no coinciden', 'error')
    }
  }

  limpiarCampos() {
    this.objRegistro.nombres = "";
    this.objRegistro.apellidos = "";
    this.objRegistro.correo = "";
    this.objRegistro.usuario = "";
    this.objRegistro.contrasena = "";
    this.objRegistro.sexo = false;
    this.confirmacion = "";
  }
  //Validaciones de campos FRONT

  validacionCamposVacios() {
    if (this.objRegistro.nombres == "" || this.objRegistro.nombres == null || this.objRegistro.nombres == undefined || this.objRegistro.apellidos == "" || this.objRegistro.apellidos == null || this.objRegistro.apellidos == undefined || this.objRegistro.correo == "" || this.objRegistro.correo == null || this.objRegistro.correo == undefined ||
      this.objRegistro.usuario == "" || this.objRegistro.usuario == null || this.objRegistro.usuario == undefined || this.objRegistro.contrasena == "" || this.objRegistro.contrasena == null || this.objRegistro.contrasena == undefined || this.objRegistro.sexo == "" || this.objRegistro.sexo == null || this.objRegistro.sexo == undefined ||
      this.confirmacion == "" || this.confirmacion == null || this.confirmacion == undefined) {
      return false;
    } else {
      if (this.validacionContrasenas() && this.msjVisibleNombre == false && this.msjVisibleApellido == false && this.msjErrorCorreo == false) {
        return true;
      } else {
        return false;
      }
    }
  }

  validacionContrasenas() {
    if (this.objRegistro.contrasena === this.confirmacion) {
      return true;
    } else {
      return false;
    }
  }

  validacionNombre() {
    var expRegular = /^[A-Za-z\s]+$/
    if (expRegular.test(this.objRegistro.nombres)) {
      this.msjVisibleNombre = false;
    } else {
      this.msjVisibleNombre = true;
    }
  }

  validacionApellido() {
    var expRegular = /^[A-Za-z\s]+$/
    if (expRegular.test(this.objRegistro.apellidos)) {
      this.msjVisibleApellido = false;
    } else {
      this.msjVisibleApellido = true;
    }
  }

  validacionCorreo() {
    var expRegular = /^[^@]+@[^@]+\.[a-zA-Z]{2,}$/
    if (expRegular.test(this.objRegistro.correo)) {
      this.msjErrorCorreo = false;
    } else {
      this.msjErrorCorreo = true;
    }
  }
}

/*/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/
openDialog(): void {
  const dialogRef = this.dialog.open(ModalIndexComponent, {
    width: '1000px',
    data: this.denuncia
  });

  dialogRef.afterClosed().subscribe(result => {
    console.log('el modal fue cerrado'); // Aqui podrias recibir datos desde el modal, con el result;
  });
}*/

