import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { IndexComponent } from './index/index.component';
import { RegistrarmeComponent } from './registrarme/registrarme.component';
import { SeguimientoComponent } from './seguimiento/seguimiento.component';
import { AdministradorComponent } from './administrador/administrador.component';
import { Page404Component } from './page404/page404.component';
import { DenunciaComponent } from './components/denuncia/denuncia-component/denuncia.component';
import { AdminClienteComponent } from './admin-cliente/admin-cliente.component';
import { FormularioAnonimoComponent } from './formulario-anonimo/formulario-anonimo.component';
import { PoliticasdeprivacidadComponent } from './politicasdeprivacidad/politicasdeprivacidad.component';
import { RestaurarPaswordComponent } from './restaurar-pasword/restaurar-pasword.component';
import { CrearpaswordComponent } from './crearpasword/crearpasword.component';
import { MisdependenciasComponent } from './misdependencias/misdependencias.component';
import { SoporteComponent } from './soporte/soporte.component';
import { MiperfilComponent } from './miperfil/miperfil.component';
import { AltaDependenciasComponent } from './alta-dependencias/alta-dependencias.component';


const routes: Routes = [
  {path:'login', component:IndexComponent},
  {path:'registro',component:RegistrarmeComponent},
  {path:'seguimiento',component:SeguimientoComponent},
  {path:'index',component:AdministradorComponent},
  {path:'denuncia',component:DenunciaComponent},
  {path:'admin',component:AdminClienteComponent},
  {path:'denunciaAnonima',component:FormularioAnonimoComponent},
  {path:'PoliticasDePrivacidad',component:PoliticasdeprivacidadComponent},
  {path:'restaurarContraseña', component:RestaurarPaswordComponent},
  {path:'crearcontraseña', component:CrearpaswordComponent},
  {path:'misdependencias', component:MisdependenciasComponent},
  {path:'soporte', component:SoporteComponent},
  {path:'miperfil',component:MiperfilComponent},
  {path:'altadependencia',component:AltaDependenciasComponent},
  {path:'**',component:Page404Component}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
