import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalFormularioAnomComponent } from './modal-formulario-anom.component';

describe('ModalFormularioAnomComponent', () => {
  let component: ModalFormularioAnomComponent;
  let fixture: ComponentFixture<ModalFormularioAnomComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalFormularioAnomComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalFormularioAnomComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
