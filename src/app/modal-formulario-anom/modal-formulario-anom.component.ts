import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-modal-formulario-anom',
  templateUrl: './modal-formulario-anom.component.html',
  styleUrls: ['./modal-formulario-anom.component.css']
})
export class ModalFormularioAnomComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<ModalFormularioAnomComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) { }

  ngOnInit() {
    console.log(this.data); // this.data es la info que llega al modal, que enviaste desde la función que lo invoca;
  }

  onNoClick(): void {
    this.dialogRef.close();
  }
}
