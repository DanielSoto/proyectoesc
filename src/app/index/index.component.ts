import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ModalIndexComponent } from '../modal-index/modal-index.component';
import $ from 'jquery';

declare var $:any;

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css']
})
export class IndexComponent implements OnInit {

  public page = "denuncia";
  denuncia:any;  // obj con el que puedes enviar información hacia el modal;

  constructor(public dialog: MatDialog) { }

 ngOnInit() {
    // this.denuncia ='Este modal si funciona :D '; // aqui puedes enviar lo que necesites, objetos, arreglos, texto;
    // this.openDialog();
}
 
// openDialog(): void {
//   const dialogRef = this.dialog.open(ModalIndexComponent, {
//     width: '1000px',
//     data: this.denuncia
//   });

//   dialogRef.afterClosed().subscribe(result => {
//     console.log('el modal fue cerrado'); // Aqui podrias recibir datos desde el modal, con el result;
//   });
// }

}
