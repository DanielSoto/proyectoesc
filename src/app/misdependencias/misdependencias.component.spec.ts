import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MisdependenciasComponent } from './misdependencias.component';

describe('MisdependenciasComponent', () => {
  let component: MisdependenciasComponent;
  let fixture: ComponentFixture<MisdependenciasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MisdependenciasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MisdependenciasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
