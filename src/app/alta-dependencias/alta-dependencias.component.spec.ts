import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AltaDependenciasComponent } from './alta-dependencias.component';

describe('AltaDependenciasComponent', () => {
  let component: AltaDependenciasComponent;
  let fixture: ComponentFixture<AltaDependenciasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AltaDependenciasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AltaDependenciasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
