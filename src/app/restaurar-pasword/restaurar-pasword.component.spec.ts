import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RestaurarPaswordComponent } from './restaurar-pasword.component';

describe('RestaurarPaswordComponent', () => {
  let component: RestaurarPaswordComponent;
  let fixture: ComponentFixture<RestaurarPaswordComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RestaurarPaswordComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RestaurarPaswordComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
