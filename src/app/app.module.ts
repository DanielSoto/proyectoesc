import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { IndexComponent } from './index/index.component';
import { DenunciaComponent } from './components/denuncia/denuncia-component/denuncia.component';
import { RegistrarmeComponent } from './registrarme/registrarme.component';
import { Page404Component } from './page404/page404.component';
import { SeguimientoComponent } from './seguimiento/seguimiento.component';
import { AdministradorComponent } from './administrador/administrador.component';
import { MatSliderModule } from '@angular/material/slider';
import {MatDialogModule} from '@angular/material/dialog';
import { DenunciaModalComponent } from './components/denuncia/denuncia-modal/denuncia-modal.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ModalStakedComponent } from './modal-staked/modal-staked.component';
import { ModalIndexComponent } from './modal-index/modal-index.component';
import { FormsModule } from '@angular/forms';
import { AdminClienteComponent } from './admin-cliente/admin-cliente.component';
import { FormularioAnonimoComponent } from './formulario-anonimo/formulario-anonimo.component';
import { ModalFormularioAnomComponent } from './modal-formulario-anom/modal-formulario-anom.component';
import { PoliticasdeprivacidadComponent } from './politicasdeprivacidad/politicasdeprivacidad.component';
import { HttpClientModule } from '@angular/common/http';
import { RestaurarPaswordComponent } from './restaurar-pasword/restaurar-pasword.component';
import { CrearpaswordComponent } from './crearpasword/crearpasword.component';
import { MisdependenciasComponent } from './misdependencias/misdependencias.component';
import { SoporteComponent } from './soporte/soporte.component';
import { MiperfilComponent } from './miperfil/miperfil.component';
import { AltaDependenciasComponent } from './alta-dependencias/alta-dependencias.component';




@NgModule({
  declarations: [
    AppComponent,
    IndexComponent,
    DenunciaComponent,
    RegistrarmeComponent,
    Page404Component,
    SeguimientoComponent,
    AdministradorComponent,
    DenunciaModalComponent,
    ModalStakedComponent,
    ModalIndexComponent,
    AdminClienteComponent,
    FormularioAnonimoComponent,
    ModalFormularioAnomComponent,
    PoliticasdeprivacidadComponent,
    RestaurarPaswordComponent,
    CrearpaswordComponent,
    MisdependenciasComponent,
    SoporteComponent,
    MiperfilComponent,
    AltaDependenciasComponent,
   
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    MatSliderModule,
    MatDialogModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [],
  entryComponents: [DenunciaModalComponent,ModalIndexComponent,ModalFormularioAnomComponent],  //AQUI SE AGG LOS MODALES PARA QUE PUEDAN FUNCIONAR
  bootstrap: [AppComponent]
})
export class AppModule { }
