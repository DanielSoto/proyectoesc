import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CrearpaswordComponent } from './crearpasword.component';

describe('CrearpaswordComponent', () => {
  let component: CrearpaswordComponent;
  let fixture: ComponentFixture<CrearpaswordComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CrearpaswordComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CrearpaswordComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
